import AppHome from "./pages/AppHome";
import Header from "./components/Header";

function App() {
  return (
    <>
      <AppHome />
    </>
  );
}

export default App;
