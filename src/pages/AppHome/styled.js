import styled from "styled-components";

export const Container = styled.div`
  background-color: #f7f8fb;

  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 100vh;

  > div:nth-child(2) {
    color: #5a5c69;
    font-size: 25px;

    position: absolute;
    left: 0;
    top: 0;

    margin-top: 45px;
    margin-left: 20px;
  }
`;
export const ContainerRender = styled.div`
  margin-bottom: -20px;
  margin-top: 45px;

  background-color: #f7f8fb;
  width: 90%;
  box-shadow: 0px 3px 55px 1px rgba(58, 59, 69, 0.15);
`;
