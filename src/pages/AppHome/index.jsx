import GetMarcas from "../../components/GetMarcas";
import GetModelos from "../../components/GetModelos";
import Header from "../../components/Header";
import { Container, ContainerRender } from "./styled";
function AppHome() {
  return (
    <>
      <Container>
        <Header />
        <div>Veículos</div>
        <ContainerRender>
          <GetMarcas />
        </ContainerRender>
        <ContainerRender>
          <GetModelos />
        </ContainerRender>
      </Container>
    </>
  );
}

export default AppHome;
