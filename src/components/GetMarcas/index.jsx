import { useEffect, useState } from "react";
import { Container, Label } from "./styled";

import Render from "../Render";
const GetMarcas = () => {
  const [brands, setBrands] = useState([]);
  const getBrands = () => {
    fetch("https://parallelum.com.br/fipe/api/v1/carros/marcas")
      .then((res) => res.json())
      .then((data) => {
        setBrands(data);
      });
  };
  useEffect(() => {
    getBrands();
  }, []);
  return (
    <Container>
      <Label>Marcas</Label>
      <Render props={brands} call={"Brands"} />
    </Container>
  );
};
export default GetMarcas;
