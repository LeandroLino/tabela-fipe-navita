import styled from "styled-components";

export const Container = styled.div`
  color: #1cc88a;
  font-size: 25px;

  width: 96%;
  height: 5vh;

  display: flex;
  justify-content: start;
  align-items: center;

  box-shadow: 0px 3px 36px -7px rgba(58, 59, 69, 0.15);

  padding-left: 50px;
  margin-bottom: 10px;

  position: absolute;
  top: 0;
`;
