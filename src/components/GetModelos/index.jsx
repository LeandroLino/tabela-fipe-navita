import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Render from "../Render";
import { Container, Label } from "./styled";
const GetModelos = () => {
  const NotInformation = [
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
    { nome: "Sem Informação..." },
  ];
  const id = useSelector((store) => store.id);
  const [models, setModels] = useState(null);
  const getModels = () => {
    id != null ? (
      fetch(`https://parallelum.com.br/fipe/api/v1/carros/marcas/${id}/modelos`)
        .then((res) => res.json())
        .then((data) => {
          setModels(data);
        })
    ) : (
      <></>
    );
  };
  useEffect(() => {
    getModels();
  }, [id]);
  return (
    <Container>
      <Label>Modelos</Label>
      {models ? (
        <Render props={models.modelos} call="Models" />
      ) : (
        <Render props={NotInformation} call="Models" />
      )}
    </Container>
  );
};
export default GetModelos;
