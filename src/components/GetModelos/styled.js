import styled from "styled-components";

export const Container = styled.div`
  background-color: #f7f8fb;

  width: 100%;

  display: flex;
  flex-flow: column;
  align-items: center;
  > div:nth-child(2) {
    > div {
      ::-webkit-scrollbar {
        width: 10px;
        height: 8px;
      }
      ::-webkit-scrollbar-thumb {
        -webkit-border-radius: 10px;
        background: grey;
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
      }

      height: 35vh;
      width: 100%;

      border: 1px solid #e3e6f0;

      align-items: flex-start;
    }
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
export const Label = styled.div`
  display: flex;
  text-align: start;
  align-items: center;

  background-color: #f7f8fb;

  font-size: 20px;
  color: #4e73df;

  width: 95%;
  height: 5vh;

  margin-bottom: 10px;
`;
