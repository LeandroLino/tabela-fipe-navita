import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  > div {
    > table {
      width: 99%;
      > thead {
        > tr {
          > th {
            font-size: 20px;
          }
        }
      }
      > tbody {
        > tr {
          > th {
            border: 1px solid rgba(224, 224, 224, 0.6);
          }
        }
      }
    }
  }
`;
export const FragmentComponent = styled.div`
  display: flex;
  justify-content: space-between;
  > div:nth-child(1) {
    width: 20%;
  }
  > button {
    border: 0;
    background-color: transparent;
    color: #4e73df;
    cursor: pointer;
  }
  > button:hover {
    color: #1cc88a;
  }
`;
