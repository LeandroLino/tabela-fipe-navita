import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { useDispatch } from "react-redux";
import { storeModels } from "../../store/modules/models/actions";
import { Container, FragmentComponent } from "./styled";
const Render = ({ props, call }) => {
  const dispatch = useDispatch();
  return (
    <Container>
      <TableContainer>
        <Table size="big" aria-label="Roboto">
          <TableHead>
            <TableRow>
              <TableCell>{call === "Brands" ? "Marcas" : "Modelos"} </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {call === "Brands"
              ? props.map((element, index) => (
                  <TableRow key={element.name}>
                    <TableCell component="th" scope="row">
                      <FragmentComponent>
                        <div>{element.nome}</div>
                        <button
                          onClick={() => dispatch(storeModels(element.codigo))}
                        >
                          ver Modelos...
                        </button>
                        <div></div>
                      </FragmentComponent>
                    </TableCell>
                  </TableRow>
                ))
              : props.map((element, index) => (
                  <TableRow key={element.name}>
                    <TableCell component="th" scope="row">
                      <div>{element.nome}</div>
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};
export default Render;
