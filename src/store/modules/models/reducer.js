export const modelsReducer = (state = null, action) => {
  switch (action.type) {
    case "@models/STORE":
      const { id } = action;
      return id;

    default:
      return state;
  }
};

export default modelsReducer;
