import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import modelsReducer from "./modules/models/reducer";

const reducers = combineReducers({
  id: modelsReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
